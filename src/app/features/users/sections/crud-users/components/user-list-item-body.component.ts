import { Component, Input } from '@angular/core';
import { User } from '../../../../../model/user';

@Component({
  selector: 'app-user-list-item-body',
  template: `
    <div *ngIf="user.address">
      <app-my-map [city]="user.address.city"></app-my-map>
      <div>{{user.address.city}}</div>
      <div>{{user.address.street}}</div>
    </div>
  `,
  styles: [
  ]
})
export class UserListItemBodyComponent {
  @Input() user!: Partial<User>;
}
