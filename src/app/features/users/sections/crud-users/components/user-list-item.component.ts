import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from '../../../../../model/user';

@Component({
  selector: 'app-user-list-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div
      class="border border-2 m-3 p-3"
      [ngClass]="{'bg-sky-300': selected}"
      (click)="setActive.emit(user)"
    >
      <div>
        {{user.name}}
        <button
          class="btn"
          (click)="deleteHandler(user.id!, $event)">Delete</button>

        <button
          *ngIf="user.address"
          class="btn"
          (click)="toggle($event)"
        >Open</button>
      </div>

      <app-user-list-item-body
        *ngIf="isOpen"
        [user]="user"
      ></app-user-list-item-body>
      
    </div>
  `,
})
export class UserListItemComponent {
  @Input() user!: Partial<User>;
  @Input() selected: boolean = false;
  @Output() setActive = new EventEmitter<Partial<User>>();
  @Output() deleteUser = new EventEmitter<number>();

  isOpen = false;

  deleteHandler(id: number, e: MouseEvent) {
    e.stopPropagation();
    this.deleteUser.emit(id)
  }

  toggle(e: MouseEvent) {
    e.stopPropagation();
    this.isOpen = !this.isOpen
  }

}
