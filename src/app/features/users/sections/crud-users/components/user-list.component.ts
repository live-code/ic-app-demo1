import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from '../../../../../model/user';

@Component({
  selector: 'app-user-list',
  template: `
    <app-user-list-item
      *ngFor="let user of users"
      [user]="user"
      [selected]="user.id === activeUser?.id"
      (deleteUser)="deleteUser.emit($event)"
      (setActive)="setActive.emit($event)"
    ></app-user-list-item>
  `,
})
export class UserListComponent {
  @Input() users: Partial<User>[] = []
  @Input() activeUser: Partial<User> | null = null;
  @Output() setActive = new EventEmitter<Partial<User>>();
  @Output() deleteUser = new EventEmitter<number>();

}
