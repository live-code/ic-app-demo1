import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { User } from '../../../../../model/user';

@Component({
  selector: 'app-user-form',
  template: `

    <form [formGroup]="form"
          (ngSubmit)="saveForm.emit(form.value)">
      <input type="text" placeholder="your name" formControlName="name">
      <input type="text" placeholder="your username" formControlName="username">
      <button type="submit" class="btn">save</button>
      <button type="button" class="btn" (click)="clearFormHandler()">clear</button>
    </form>
  `,
})
export class UserFormComponent {
  @Input() set activeUser(user: Partial<User> | null) {
    if (user?.id) {
      this.form.patchValue(user)
    } else {
      this.form.reset();
    }
  };
  @Output() clearForm = new EventEmitter()
  @Output() saveForm = new EventEmitter<any>()

  form = new FormGroup<any>({
    name: new FormControl(''),
    username: new FormControl('')
  })


  clearFormHandler() {
    this.clearForm.emit()
    this.form.reset();
  }

}
