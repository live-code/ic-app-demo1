import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../../../../../model/user';
import { UpsertUser } from '../crud-users.component';


@Injectable({ providedIn: 'root' })
export class UsersService {
  users: Partial<User>[] = [];
  activeUser: Partial<User> = {};

  constructor(private http: HttpClient) {}

  getUsers() {
    this.http.get<User[]>('https://jsonplaceholder.typicode.com/users')
      .subscribe((res) => {
        this.users = res;
      })
  }

  saveUser(user: Partial<User>) {
    if (this.activeUser?.id) {
      return this.editUser(user);
    } else {
      return this.addUser(user);
    }
  }

  addUser(user: Partial<User>): Promise<{ result: UpsertUser, action: 'add' | 'edit'}> {
    return new Promise((resolve, reject) => {
      this.http.post<UpsertUser>('https://jsonplaceholder.typicode.com/users', user)
        .subscribe((res) => {
          // this.users.push(res);
          // this.users = this.users.concat(res)
          this.users = [...this.users, res]
          this.activeUser = {};
          resolve({ result: res, action: 'add' });
        })
    })

  }

  editUser(user: Partial<User>): Promise<{ result: UpsertUser, action: 'add' | 'edit'}> {
    return new Promise((resolve, reject) => {
      this.http.patch<UpsertUser>(`https://jsonplaceholder.typicode.com/users/${this.activeUser?.id}`, user)
        .subscribe((res) => {
          this.users = this.users.map(u => {
            return u.id === this.activeUser?.id ? res : u;
          })
          resolve({ result: res, action: 'edit' });
        })
    })
  }

  setActiveUser(user: Partial<User>) {
    this.activeUser = user;
  }

  clearForm() {
    this.activeUser = { };
  }

  deleteUser(id: number): Promise<void> {
    return new Promise((resolve, reject) => {
      this.http.delete(`https://jsonplaceholder.typicode.com/users/${id}`)
        .subscribe(
          () => {
            /*const index = this.users.findIndex(u => u.id === id);
            this.users.splice(index, 1);*/
            this.users = this.users.filter(u => u.id !== id)

            if (id === this.activeUser?.id) {
              this.clearForm();
              resolve()
            }

          }
        )
    })
  }

}
