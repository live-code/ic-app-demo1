import { Component, OnInit } from '@angular/core';
import { User } from '../../../../model/user';
import { UsersService } from './services/users.service';

export type UpsertUser = Pick<User, 'id' | 'name' | 'username'>

@Component({
  selector: 'app-crud-users',
  template: `
    <app-user-form
      [activeUser]="usersService.activeUser"
      (saveForm)="usersService.saveUser($event)"
      (clearForm)="usersService.clearForm()"
    ></app-user-form>
      
   <app-user-list
     [users]="usersService.users"
     [activeUser]="usersService.activeUser"
     (setActive)="usersService.setActiveUser($event)"
     (deleteUser)="usersService.deleteUser($event)"
   ></app-user-list>
  `,
})
export class CrudUsersComponent implements OnInit {

  constructor(public usersService: UsersService) { }

  ngOnInit() {
    this.usersService.getUsers()
  }
}
