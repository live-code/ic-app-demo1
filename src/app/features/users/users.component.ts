import { Component } from '@angular/core';


@Component({
  selector: 'app-users',
  template: `
    <h1>USER PAGE</h1>
    <app-crud-users></app-crud-users>
  `,
})
export class UsersComponent  {

}
