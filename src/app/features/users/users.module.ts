import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { CrudUsersComponent } from './sections/crud-users/crud-users.component';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { UserListComponent } from './sections/crud-users/components/user-list.component';
import { UserFormComponent } from './sections/crud-users/components/user-form.component';
import { UserListItemComponent } from './sections/crud-users/components/user-list-item.component';
import { UserListItemBodyComponent } from './sections/crud-users/components/user-list-item-body.component';


@NgModule({
  declarations: [
    UsersComponent,
    CrudUsersComponent,
    UserListComponent,
    UserFormComponent,
    UserListItemComponent,
    UserListItemBodyComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    UsersRoutingModule
  ]
})
export class UsersModule { }
