import { Component } from '@angular/core';
import { ThemeService } from '../../core/services/theme.service';

@Component({
  selector: 'app-home',
  template: `
    <p>
      home works!
    </p>
    <button class="btn" (click)="themeService.theme = 'light'">light</button>
    <button class="btn" (click)="themeService.theme = 'dark'">dark</button>
  `,
  styles: [
  ]
})
export class HomeComponent {
  constructor(public themeService: ThemeService) {
  }
}
