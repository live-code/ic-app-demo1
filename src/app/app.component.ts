import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <app-navbar></app-navbar>
  
    <hr>
    <router-outlet></router-outlet>
  `,
})
export class AppComponent {
  title = 'ic-app-demo1';
}
