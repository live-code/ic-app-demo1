import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Theme } from '../../model/theme';


@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  private value: Theme = 'dark';

  constructor() {
    const currentTheme = localStorage.getItem('theme')
    if (currentTheme) {
      this.value = currentTheme as Theme;
    }
  }

  set theme(newTheme: Theme) {
    this.value = newTheme;
    localStorage.setItem('theme', newTheme)
  }

  get theme(): Theme {
    return this.value;
  }

  get isDark(): boolean {
    return this.value === 'dark';
  }

  get isLight(): boolean {
    return this.value === 'light';
  }

}
