import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ThemeService } from '../services/theme.service';

@Component({
  selector: 'app-navbar',
  template: `
    <div 
      class="my-3"
      [ngClass]="{
        'dark': themeService.isDark,
        'light': themeService.isLight
      }"
    >
      <button class="btn" routerLink="home">home</button>
      <button class="btn" routerLink="users">users</button>
    </div>
    <hr>
  `,
  styles: [`
    .dark { background-color: #222; color: white}
    .light { background-color: #ccc; color: black}
  `]
})
export class NavbarComponent {

  constructor(public themeService: ThemeService) {
  }
}
