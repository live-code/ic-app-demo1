import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyMapComponent } from './components/my-map.component';



@NgModule({
  declarations: [
    MyMapComponent,
  ],
  exports: [
    MyMapComponent
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
